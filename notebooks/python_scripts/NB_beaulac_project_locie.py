#! usr/bin/env python3
#  -*- coding: utf-8 -*-
from omegalpes.energy.units.consumption_units import *
from omegalpes.energy.units.production_units import *
from omegalpes.energy.units.conversion_units import *
from omegalpes.energy.units.storage_units import *
from omegalpes.energy.energy_nodes import *
from omegalpes.energy.exergy import *
from omegalpes.general.time import *
from omegalpes.general.optimisation.model import *
from omegalpes.general.utils.plots import *
from omegalpes.general.utils.output_data import save_energy_flows
from pulp import LpStatus, GUROBI_CMD
import time as timer


def beaulac_project_init(work_path, exergy_analysis=False, time_lapse=8760,
          temp_dead_state=20,
         pv_area=1, pv_efficiency=0, st_area=1, st_efficiency=0,
         st_temp_out=100, elec_batt_pc_max=1, elec_batt_pd_max=1,
         elec_batt_capa=1, elec_batt_soc_min=1, elec_batt_soc_max=1,
         elec_batt_self_disch=1, elec_batt_soc_ini=1, therm_stor_pc_max=1,
         therm_stor_pd_max=1, therm_stor_capa=1, therm_stor_soc_min=1,
         therm_stor_soc_max=1, therm_stor_self_disch=1, therm_stor_soc_ini=1,
         therm_stor_temp=65, p_max_lake=100, conduit_max_cooling=100,
         hp_heating_cop=2, hp_heating_pmax_elec=1, hp_heating_temp_in=25,
         hp_heating_temp_out=50, hp_dhw_cop=2, hp_dhw_pmax_elec=1,
         hp_dhw_temp_in=25, hp_dhw_temp_out=50, hp_cooling_cop=2,
         hp_cooling_pmax_elec=1, hp_cooling_temp_in=25, hp_cooling_temp_out=50,
         fluid_pumps_exergy_eff=1, high_heat_diss_temp=70,
         medium_heat_diss_temp=35, low_heat_diss_temp=15,
         heat_diss_exergy_eff=0):

    time = TimeUnit(periods=time_lapse, dt=1)

    if time_lapse == 24:
        conduit_hydraulics_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day"
                        "/conduit_hydraulics_elec_cons_1_day.txt",
            "r")
        conduit_hydraulics_cons = [c for c in
                                   map(float, conduit_hydraulics_file)]
        hameau_dhw_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day"
                        "/hameau_dhw_cons_1_day.txt",
            "r")
        hameau_dhw_cons = [c for c in map(float, hameau_dhw_file)]
        hameau_elec_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/hameau_elec_cons_1_day.txt",
            "r")
        hameau_elec_cons = [c for c in map(float, hameau_elec_file)]
        hameau_heating_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/hameau_heating_cons_1_day.txt",
            "r")
        hameau_heating_cons = [c for c in map(float, hameau_heating_file)]
        ines_cooling_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/ines_cooling_cons_1_day.txt",
            "r")
        ines_cooling_prod = [c for c in map(float, ines_cooling_file)]
        solar_irradiation_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/solar_irradiation_1_day.txt",
            "r")
        solar_pv_input = [c * pv_area for c in
                          map(float, solar_irradiation_file)]

        solar_irradiation_file2 = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/solar_irradiation_1_day.txt",
            "r")
        solar_thermal_input = [c * st_area for c in
                               map(float, solar_irradiation_file2)]
        zac_2_cooling_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/zac_2_cooling_cons_1_day.txt",
            "r")
        zac_2_cooling_prod = [c for c in map(float, zac_2_cooling_file)]
        zac_2_dhw_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/zac_2_dhw_cons_1_day.txt",
            "r")
        zac_2_dhw_cons = [c for c in map(float, zac_2_dhw_file)]
        zac_2_elec_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/zac_2_elec_cons_1_day.txt",
            "r")
        zac_2_elec_cons = [c for c in map(float, zac_2_elec_file)]
        zac_2_heating_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/zac_2_heating_cons_1_day.txt",
            "r")
        zac_2_heating_cons = [c for c in map(float, zac_2_heating_file)]
        zac_3_cooling_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/zac_3_cooling_cons_1_day.txt",
            "r")
        zac_3_cooling_prod = [c for c in map(float, zac_3_cooling_file)]
        zac_3_dhw_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/zac_3_dhw_cons_1_day.txt",
            "r")
        zac_3_dhw_cons = [c for c in map(float, zac_3_dhw_file)]
        zac_3_elec_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/zac_3_elec_cons_1_day.txt",
            "r")
        zac_3_elec_cons = [c for c in map(float, zac_3_elec_file)]
        zac_3_heating_file = open(
            work_path + "/data/beaulac_data_profiles/Profiles/1_day/zac_3_heating_cons_1_day.txt",
            "r")
        zac_3_heating_cons = [c for c in map(float, zac_3_heating_file)]
    elif time_lapse == 8760:

        conduit_hydraulics_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/conduit_hydraulics_elec_cons_1_year.txt",

            "r")

        conduit_hydraulics_cons = [c for c in

                                   map(float, conduit_hydraulics_file)]

        hameau_dhw_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/hameau_dhw_cons_1_year.txt",
            "r")

        hameau_dhw_cons = [c for c in map(float, hameau_dhw_file)]

        hameau_elec_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/hameau_elec_cons_1_year.txt",
            "r")

        hameau_elec_cons = [c for c in map(float, hameau_elec_file)]

        hameau_heating_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/hameau_heating_cons_1_year.txt",

            "r")

        hameau_heating_cons = [c for c in map(float, hameau_heating_file)]

        ines_cooling_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/ines_cooling_cons_1_year.txt",
            "r")

        ines_cooling_prod = [c for c in map(float, ines_cooling_file)]

        solar_irradiation_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/solar_irradiation_1_year.txt",
            "r")

        solar_pv_input = [c * pv_area for c in

                          map(float, solar_irradiation_file)]

        solar_irradiation_file2 = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/solar_irradiation_1_year.txt",
            "r")

        solar_thermal_input = [c * st_area for c in

                               map(float, solar_irradiation_file2)]

        zac_2_cooling_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/zac_2_cooling_cons_1_year.txt",
            "r")

        zac_2_cooling_prod = [c for c in map(float, zac_2_cooling_file)]

        zac_2_dhw_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/zac_2_dhw_cons_1_year.txt",
            "r")

        zac_2_dhw_cons = [c for c in map(float, zac_2_dhw_file)]

        zac_2_elec_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/zac_2_elec_cons_1_year.txt",
            "r")

        zac_2_elec_cons = [c for c in map(float, zac_2_elec_file)]

        zac_2_heating_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/zac_2_heating_cons_1_year.txt",
            "r")

        zac_2_heating_cons = [c for c in map(float, zac_2_heating_file)]

        zac_3_cooling_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/zac_3_cooling_cons_1_year.txt",
            "r")

        zac_3_cooling_prod = [c for c in map(float, zac_3_cooling_file)]

        zac_3_dhw_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/zac_3_dhw_cons_1_year.txt",
            "r")

        zac_3_dhw_cons = [c for c in map(float, zac_3_dhw_file)]

        zac_3_elec_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/zac_3_elec_cons_1_year.txt",
            "r")

        zac_3_elec_cons = [c for c in map(float, zac_3_elec_file)]

        zac_3_heating_file = open(

            work_path + "/data/beaulac_data_profiles/Profiles/1_year/zac_3_heating_cons_1_year.txt",
            "r")

        zac_3_heating_cons = [c for c in map(float, zac_3_heating_file)]
    else:
        raise(ValueError, 'Only time lapses of either 24 h or 8760 h are '
                         'allowed.')

    # *** CREATING THE UNITS ***

    lake = VariableProductionUnit(
        time, name='lake', energy_type='Thermal', p_max=p_max_lake)

    buy_elec_from_grid = VariableProductionUnit(
        time, name='buy_elec_from_grid', energy_type='Electrical')
    sell_elec_to_grid = VariableConsumptionUnit(
        time, name='sell_elec_to_grid', energy_type='Electrical')
    zac_2_elec = FixedConsumptionUnit(
        time, name='zac_2_elec', p=zac_2_elec_cons, energy_type='Electrical')
    zac_3_elec = FixedConsumptionUnit(
        time, name='zac_3_elec', p=zac_3_elec_cons, energy_type='Electrical')
    hameau_elec = FixedConsumptionUnit(
        time, name='hameau_elec', p=hameau_elec_cons, energy_type='Electrical')
    conduit_hydraulics = FixedConsumptionUnit(
        time, name='conduit_hydraulics', p=[0]*time_lapse, energy_type='Electrical')

    zac_2_dhw = FixedConsumptionUnit(
        time, name='zac_2_dhw', p=zac_2_dhw_cons, energy_type='Thermal')
    zac_3_dhw = FixedConsumptionUnit(
        time, name='zac_3_dhw', p=zac_3_dhw_cons, energy_type='Thermal')
    hameau_dhw = FixedConsumptionUnit(
        time, name='hameau_dhw', p=hameau_dhw_cons, energy_type='Thermal')

    zac_2_heating = FixedConsumptionUnit(
        time, name='zac_2_heating', p=zac_2_heating_cons, energy_type='Thermal')
    zac_3_heating = FixedConsumptionUnit(
        time, name='zac_3_heating', p=zac_3_heating_cons, energy_type='Thermal')
    hameau_heating = FixedConsumptionUnit(
        time, name='hameau_heating', p=hameau_heating_cons, energy_type='Thermal')

    solar_irradiation_pv = FixedProductionUnit(
        time, name='solar_irradiation_pv', p=solar_pv_input, energy_type='Electrical')
    solar_irradiation_st = FixedProductionUnit(
        time, name='solar_irradiation_st', p=solar_thermal_input, energy_type='Electrical')

    zac_2_cooling = FixedProductionUnit(
        time, name='zac_2_cooling', p=zac_2_cooling_prod, energy_type='Thermal')
    zac_3_cooling = FixedProductionUnit(
        time, name='zac_3_cooling', p=zac_3_cooling_prod, energy_type='Thermal')
    ines_cooling = FixedProductionUnit(
        time, name='ines_cooling', p=ines_cooling_prod, energy_type='Thermal')

    elec_batteries = StorageUnit(time, name='elec_batteries',
                                 pc_max=elec_batt_pc_max,
                                 pd_max=elec_batt_pd_max,
                                 capacity=elec_batt_capa,
                                 soc_min=elec_batt_soc_min,
                                 soc_max=elec_batt_soc_max,
                                 self_disch_t=elec_batt_self_disch,
                                 e_0=elec_batt_soc_ini,
                                 # ef_is_e0=True,
                                 energy_type='Electrical')

    thermal_storage = StorageUnit(time, name='thermal_storage',
                                  pc_max=therm_stor_pc_max,
                                  pd_max = therm_stor_pd_max,
                                  capacity = therm_stor_capa,
                                  soc_min = therm_stor_soc_min,
                                  soc_max = therm_stor_soc_max,
                                  self_disch_t = therm_stor_self_disch,
                                  e_0 = therm_stor_soc_ini,
                                  # ef_is_e0=True,
                                  energy_type='Thermal')

    high_temp_heat_dissipation = VariableConsumptionUnit(
        time, name='high_temp_heat_dissipation', energy_type='Thermal')

    medium_temp_heat_dissipation = VariableConsumptionUnit(
        time, name='medium_temp_heat_dissipation', energy_type='Thermal')

    low_temp_heat_dissipation = VariableConsumptionUnit(
        time, name='low_temp_heat_dissipation', energy_type='Thermal')

    pv_panels = ElectricalConversionUnit(
        time, name='pv_panels', elec_to_elec_ratio=pv_efficiency)

    solar_thermal_collectors = ElectricalToThermalConversionUnit(
        time, name='solar_thermal_collectors', elec_to_therm_ratio=st_efficiency)

    heat_pump_heating = HeatPump(time, name='heat_pump_heating',
                                 cop=hp_heating_cop,
                                 pmax_in_elec=hp_heating_pmax_elec)

    heat_pump_dhw = HeatPump(time, name='heat_pump_dhw',
                             cop=hp_dhw_cop,  pmax_in_elec=hp_dhw_pmax_elec)

    heat_pump_cooling = HeatPump(time, name='heat_pump_cooling',
                                 cop=hp_cooling_cop,
                                 pmax_in_elec=hp_cooling_pmax_elec)

    imaginary_elec_prod_unit = VariableProductionUnit(time, name='imaginary_elec_prod_unit', energy_type='Electrical')
    from_high_temp_to_medium_temp = HeatPump(time, name='from_high_temp_to_medium_temp', cop=1e10, pmax_out_therm=1e4)

    return lake, buy_elec_from_grid, sell_elec_to_grid, zac_2_elec, zac_3_elec, hameau_elec, conduit_hydraulics, zac_2_dhw, \
           zac_3_dhw, hameau_dhw, zac_2_heating, zac_3_heating, hameau_heating, solar_irradiation_pv, solar_irradiation_st, zac_2_cooling, \
           zac_3_cooling, ines_cooling, elec_batteries, high_temp_heat_dissipation, medium_temp_heat_dissipation, low_temp_heat_dissipation, \
           thermal_storage, pv_panels, solar_thermal_collectors, heat_pump_dhw, heat_pump_heating, heat_pump_cooling, from_high_temp_to_medium_temp, imaginary_elec_prod_unit  # hp_dhw_elec_inlet_node, hp_heating_elec_inlet_node, hp_cooling_elec_inlet_node, from_cooling_to_conduit,